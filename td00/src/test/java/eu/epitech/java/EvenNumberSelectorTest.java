package eu.epitech.java;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class EvenNumberSelectorTest {

	@Test
	public void test() {
	EvenNumberSelector selector = new EvenNumberSelector();
	List<Integer> list = new ArrayList<Integer>();
	
	list.add(1);
	list.add(2);
	list.add(3);
	
	list = Lists.newArrayList(Iterables.filter(list, selector));
	
	assertEquals(1, list.size());
	assertEquals(new Integer(2), list.get(0));
	
	}

}
