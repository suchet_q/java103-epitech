package net.epitech.java.td03.dao;

import java.util.Collection;

import net.epitech.java.td03.exception.SmartCardException;
import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;

public interface SmartCardDAO {

	// CREATE
	/**
	 * persis a new smartCard to Database
	 * 
	 * @param card
	 */
	public void save(SmartCard card);

	/**
	 * retreive persisted Smartcard for a given type
	 * 
	 * @param type
	 *            a smartcard type
	 * @return a collection of Smartcard corresponding to the supplied type
	 * @throws SmartCardException if an error occures
	 */
	public Collection<SmartCard> getSmartCardByType(SmartCardType type) throws SmartCardException;

	/**
	 * retreive persisted Smartcard for a giver holder name
	 * 
	 * @param holderName
	 *            full name of the holder
	 * @return a collection of Smartcard corresponding to the supplied holder
	 *         Name
	 */
	public Collection<SmartCard> getSmartCardByNmae(String holderName);

	/**
	 * for a currently persisted Smartcard, update the DB reccord
	 * 
	 * @param card
	 *            the smartcard object to be updated
	 */
	public void update(SmartCard card);

	/**
	 * Delete a smartcard from the database
	 * 
	 * @param id
	 */
	public void delete(int id);

}
